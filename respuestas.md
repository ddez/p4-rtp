## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268 paquetes
* ¿Cuánto tiempo dura la captura?:12,8 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 100 por segundo
* ¿Qué protocolos de nivel de red aparecen en la captura?: ip
* ¿Qué protocolos de nivel de transporte aparecen en la captura?:udp 
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?:rtp
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N):304
* Dirección IP de la máquina A:216.234.64.16
* Dirección IP de la máquina B:192.168.0.10
* Puerto UDP desde el que se envía el paquete:54550
* Puerto UDP hacia el que se envía el paquete:59154
* SSRC del paquete:0X31BE1E0E
* Tipo de datos en el paquete RTP (según indica el campo correspondiente):ITU-T G.711 PCMU (0)

## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?:4
* ¿Cuántos paquetes hay en el flujo F?:626 paquetes
* ¿El origen del flujo F es la máquina A o B?: La máquina B
* ¿Cuál es el puerto UDP de destino del flujo F?:49154
* ¿Cuántos segundos dura el flujo F?:12.49 segundos
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 21.187000 ms
* ¿Cuál es el jitter medio del flujo?:0.229065
* ¿Cuántos paquetes del flujo se han perdido?: 0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: aproximadamente o.5
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?:18587
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: se retrasa 9 segundos más tarde
* ¿Qué jitter se ha calculado para ese paquete? 0.012 seg
* ¿Qué timestamp tiene ese paquete? 1769329803
* ¿Por qué número hexadecimal empieza sus datos de audio? fe fe fe fe
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`:vie 20 oct 2023 10:06:11 CEST
* Número total de paquetes en la captura:6670
* Duración total de la captura (en segundos):122 segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?:450 paquetes
* ¿Cuál es el SSRC del flujo?:0x8b10809c
* ¿En qué momento (en ms) de la traza comienza el flujo? 102000
* ¿Qué número de secuencia tiene el primer paquete del flujo?31539
* ¿Cuál es el jitter medio del flujo?: 0 
* ¿Cuántos paquetes del flujo se han perdido?:0
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?:dborja
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): 5290 paquetes de menos
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: los mismos, 450 paquetes
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: 12940 
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: SSRC, sourceport, el start time y varían ligeramente los deltas (cerca de 0,01 ms de diferencia)
